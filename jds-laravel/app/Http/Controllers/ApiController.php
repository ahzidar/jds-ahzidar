<?php

namespace App\Http\Controllers;

use JWTAuth;
use App\Models\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

class ApiController extends Controller
{
    /**
     * @OA\Post(
     *      path="/login",
     *      tags={"Authentication"},
     *      summary="Login",
     *      description="Returns User Data & JWT Token",
     *      @OA\RequestBody(
     *          required=true,
     *          description="Pass user credentials",
     *          @OA\JsonContent(
     *              required={"nik","role", "password"},
     *              @OA\Property(property="nik", type="string", example="1234567890123456"),
     *              @OA\Property(property="role", type="string", example="SUPER ADMIN"),
     *              @OA\Property(property="password", type="string", format="password", example="12345678"),
     *          ),
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *       ),
     * )
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('nik', 'role', 'password');

        //valid credential
        $validator = Validator::make($credentials, [
            'nik' => 'required|string|min:16|max:16',
            'role' => 'required|string',
            'password' => 'required|string|min:6|max:8'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        //Request is validated
        //Creat token
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                	'success' => false,
                	'message' => 'Login credentials are invalid.',
                ], 400);
            }
        } catch (JWTException $e) {
    	return $credentials;
            return response()->json([
                	'success' => false,
                	'message' => 'Could not create token.',
                ], 500);
        }

        //Get Info User
        $user = User::where('nik', $request->nik)->first();

 		//Token created, return with success response and jwt token
        return response()->json([
            'success' => true,
            'id' => $user->id,
            'nik' => $user->nik,
            'role' => $user->role,
            'token' => $token,
        ]);
    }

    /**
     * @OA\Get(
     *      path="/logout/{token}",
     *      tags={"Authentication"},
     *      security={{ "apiAuth": {} }},
     *      summary="Logout",
     *      description="Logout App",
     *      @OA\Parameter(
     *          name="token",
     *          description="jwt token",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *     )
     */
    public function logout(Request $request)
    {

        if($request->token){
            //Request is validated, do logout
            try {
                JWTAuth::invalidate($request->token);

                return response()->json([
                    'success' => true,
                    'message' => 'User has been logged out'
                ]);
            } catch (JWTException $exception) {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, user cannot be logged out',
                    'message_error' => $exception->getMessage()
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        } else {
            return response()->json(['error' => 'Invalid Token'], 200);
        }

    }
}
