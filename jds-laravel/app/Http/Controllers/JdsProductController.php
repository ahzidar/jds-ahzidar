<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\JdsProduct;
use DB;

class JdsProductController extends Controller
{
    // Get Convert Amount From Different Currency
    private function convertCurrency($from_currency,$to_currency){
        $apikey = '8f4fa64869b721ef3b4b';

        $from_Currency = urlencode($from_currency);
        $to_Currency = urlencode($to_currency);
        $query =  "{$from_Currency}_{$to_Currency}";

        // fetch covert amount from free.currcony
        $json = file_get_contents("https://free.currconv.com/api/v7/convert?q={$query}&compact=ultra&apiKey={$apikey}");
        $obj = json_decode($json, true);

        $val = floatval($obj["$query"]);

        // return convert amount
        return $val;

    }

    // Get Data Fron Jds Product
    private function fetchingData(){
        $client = new \GuzzleHttp\Client();
        $urlapi_product = 'https://60c18de74f7e880017dbfd51.mockapi.io/api/v1/jabar-digital-services/product';

        // fetch data form jds product
        $response = $client->request('GET', $urlapi_product, [
        'headers' => [
            'accept' => 'application/json',
            'content-type' => 'application/json',
        ],
        'verify' => false]);

        $response = $response->getBody()->getContents();
        $obj = json_decode($response);

        // return jds product
        return $obj;

    }

    /**
     * @OA\Get(
     *      path="/fetch_data",
     *      tags={"Jds Products"},
     *      security={{ "apiAuth": {} }},
     *      summary="Get JDS Products",
     *      description="Returns list of JDS Products",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *     )
     */

    // Get Data Fron Jds Product
    public function fetchData(Request $request)
    {
        try {

            //Initialization New Array Of Data
            $new_data = [];

            //Get Convert Amount & Data Product
            $convertAmount = $this->convertCurrency('USD', 'IDR');
            $data = $this->fetchingData();

            //Add New Field priceIDR & Push New Data to new_data
            foreach ($data as $item){
                $amountIDR = $item->price * $convertAmount;
                // $amountIDR = number_format($amountIDR, 2, ',','.');
                $newItem = [
                    "id" => $item->id,
                    "createdAt" => $item->createdAt,
                    "priceUSD" => $item->price,
                    "priceIDR" => $amountIDR,
                    "department" => $item->department,
                    "product" => $item->product
                ];
                $new = array_push($new_data, $newItem);

                // Checking If Data Already Exist
                $data = JdsProduct::find($item->id);

                // Start Initialization to DB
                DB::beginTransaction();

                try{

                    if ($data) {

                        // Create Data If Data Is Not Exist
                        $data->createdat = $item->createdAt;
                        $data->priceusd = $item->price;
                        $data->priceidr = $amountIDR;
                        $data->department = $item->department;
                        $data->product = $item->product;
                        $data->update();

                    } else {

                        // Update Data If Data Is Exist
                        $jdsProduct = new JdsProduct;
                        $jdsProduct->createdat = $item->createdAt;
                        $jdsProduct->priceusd = $item->price;
                        $jdsProduct->priceidr = $amountIDR;
                        $jdsProduct->department = $item->department;
                        $jdsProduct->product = $item->product;
                        $jdsProduct->save();
                    }

                    // Commit to DB
                    DB::commit();

                } catch (\Exception $e) {

                    // Rollback
                    DB::rollBack();
                    \Log::error($e->getMessage());
                    return response()->json([
                        'success' => false,
                        'message' => $e->getMessage(),
                    ], 500);
                }

            }

            // return new_data
            return response()->json([
                'success' => true,
                'data' => $new_data,
            ], 200);

        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 500);
        }

    }

    /**
     * @OA\Get(
     *      path="/aggregate_data/{aggregateBy}",
     *      tags={"Jds Products"},
     *      security={{ "apiAuth": {} }},
     *      summary="Get Aggregate Jds Products",
     *      description="Returns Aggregate Jds Product",
     *      @OA\Parameter(
     *          name="aggregateBy",
     *          description="department or product",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *     )
     */
    public function aggregateData(Request $request)
    {
        try {
            // Get Paramater Aggregation
            $aggregateBy = $request->aggregateBy;
            // return $aggregateBy;

            // Aggregation For Department
            if ($aggregateBy == 'department'){
                $data = JdsProduct::select(
                    'jds_products.department',
                    DB::raw('SUM(jds_products.priceidr) as totalpriceidr'),
                    DB::raw('AVG(jds_products.priceidr) as averagepriceidr'),
                    DB::raw('MAX(jds_products.priceidr) as maxpriceidr'),
                    DB::raw('MIN(jds_products.priceidr) as maxpriceidr'),
                )
                ->groupBy('jds_products.department')
                ->orderBy('totalpriceidr', 'asc')
                ->get();

            // Aggregation For Product
            } elseif ($aggregateBy == 'product'){
                $data = JdsProduct::select(
                    'jds_products.product',
                    DB::raw('SUM(jds_products.priceidr) as totalpriceidr'),
                    DB::raw('AVG(jds_products.priceidr) as averagepriceidr'),
                    DB::raw('MAX(jds_products.priceidr) as maxpriceidr'),
                    DB::raw('MIN(jds_products.priceidr) as maxpriceidr'),
                )
                ->groupBy('jds_products.product')
                ->orderBy('totalpriceidr', 'asc')
                ->get();

            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'aggregateBy is not correct.',
                ], 200);
            }

            return response()->json([
                'success' => true,
                'data' => $data,
            ], 200);

        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 500);
        }
    }
}
