<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\JdsProductController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, enctype');
header('Access-Control-Allow-Methods: GET, PATCH, POST, DELETE');

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::middleware(['cors'])->group(function () {
    Route::post('login', [ApiController::class, 'authenticate']);

    Route::group(['middleware' => ['jwt.verify']], function() {
        Route::get('logout/{token}', [ApiController::class, 'logout']);
        Route::get('fetch_data', [JdsProductController::class, 'fetchData']);
        Route::get('aggregate_data/{aggregateBy}', [JdsProductController::class, 'aggregateData']);
    });
});

