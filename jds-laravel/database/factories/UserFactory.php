<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

use App\Models\User;

class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = User::class;

    public function definition()
    {
        return [
            'nik' => $this->faker->unique()->numerify('################'),
            'role' => $this->faker->randomElement(['ADMIN', 'SUPER ADMIN', 'OPERATION']),
            'password' => '$2y$10$WCBOP045.N9btn5vy4vsmO.P2UJjJmnrXE0lMFySDY63IS4a2v7dK', // password
            'remember_token' => Str::random(10),
        ];
    }
}
