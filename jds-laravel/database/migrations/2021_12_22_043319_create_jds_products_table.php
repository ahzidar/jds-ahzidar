<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJdsProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jds_products', function (Blueprint $table) {
            $table->id();
            $table->float('priceusd', 100, 2);
            $table->float('priceidr', 100, 2);
            $table->string('product');
            $table->string('department');
            $table->string('createdat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jds_products');
    }
}
