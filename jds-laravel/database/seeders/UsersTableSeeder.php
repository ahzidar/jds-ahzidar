<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'nik' => '1234567890123456',
            'role' => 'SUPER ADMIN',
            'password' => '$2y$10$WCBOP045.N9btn5vy4vsmO.P2UJjJmnrXE0lMFySDY63IS4a2v7dK',
        ]);
        User::factory()->count(10)->create();

    }
}
