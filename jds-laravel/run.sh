#!/bin/sh
echo "Start Compose"
# cd /var/www
chmod -R 777 /app/bootstrap
chmod -R 777 /app/storage
composer install
php artisan key:generate
php artisan config:cache
php artisan cache:clear
php artisan config:clear
php artisan migrate:fresh --seed
php artisan jwt:secret
php artisan l5-swagger:generate
echo "Done"

