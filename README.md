# JDS AHZIDAR

## How to setup 

This is a guide how to clone the project until running the app inside the docker.

### Requirements

1. You must have access to all repos that were written inside [`.gitmodules`](https://gitlab.com/ihsansolusi/universal-front-end/pos-project/-/blob/bns/dev/.gitmodules) file. If you don't have please ask the `maintainer` to give you permission.
1. [Docker](https://docs.docker.com/get-docker/) installed on your machine.

## Installation
### 1. Clone Repository

```bash
# clone all branches inside repo
git clone https://gitlab.com/ahzidar/jds-ahzidar.git
```

### 2. Setup Environment

```bash
# copy environment sample to .env
cp .env.sample .env
setting L5_SWAGGER_CONST_HOST to host that you want to deploy

# build all dockerfile from image also run after completed.
docker-compose up -d --build

# migrate db from laravel container
docker exec -it jds-laravel /bin/sh
php artisan migrate:fresh --seed
```

### 3. Run Containers and Start Development
```bash
# start all service on docker-compose.yml
docker-compose up -d

# Jds-laravel for db scenario
To see web app laravel api, go to [`http:localhost:3000/api/documentation`](http:localhost:3000/api/documentation). 

# Jds-fastapi for non db scenario
To see web app fastapi, go to [`http:localhost:8000/docs`](http:localhost:8000/docs).

To access database, you can access from [`http:localhost:5432`](http:localhost:5432) 
database : jds_product
username : root
password : root

Happy Coding :)
