from pydantic import BaseModel


class AuthDetails(BaseModel):
    nik: str
    role: str
    password: str
