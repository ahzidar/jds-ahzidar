from fastapi import FastAPI, Depends, HTTPException
# from pydantic.types import Json
from starlette.requests import Request
from .auth import AuthHandler
from .schemas import AuthDetails
from fastapi.openapi.utils import get_openapi
import requests
import json


app = FastAPI()


auth_handler = AuthHandler()
users = []

@app.get("/")
def read_root():
    return {
                "Jds" : "ahzidar fastapi",
                "doc" : "please go to http://178.128.94.160:8000/docs"
           }

@app.post('/register', status_code=201)
def register(auth_details: AuthDetails):
    if any(x['nik'] == auth_details.nik for x in users):
        raise HTTPException(status_code=400, detail='NIK is taken')
    if len(auth_details.nik) != 16 :
        raise HTTPException(status_code=400, detail='NIK must contain 16 digits')
    hashed_password = auth_handler.get_password_hash(auth_details.password)
    users.append({
        'id' : len(users)+1,
        'nik': auth_details.nik,
        'role': auth_details.role,
        'password': hashed_password    
    })
    return { 
                'status': True,
                'id'    : len(users)+1,
                'nik'   : auth_details.nik,
                'role'  : auth_details.role,
            }


@app.post('/login', status_code=200)
def login(auth_details: AuthDetails):
    user = None
    for x in users:
        if x['nik'] == auth_details.nik:
            user = x
            break
    
    if (user is None) or (not auth_handler.verify_password(auth_details.password, user['password'])):
        raise HTTPException(status_code=401, detail='Invalid nik and/or password')
    token = auth_handler.encode_token(user['nik'])
    return { 
                'status' : True,
                'id'     : user['id'],
                'nik'    : user['nik'],
                'role'   : user['role'],
                'token'  : token 
           }


@app.get('/fetch_data')
def protected(nik=Depends(auth_handler.auth_wrapper)):
    api_convert_currency = f"https://free.currconv.com/api/v7/convert?q=USD_IDR&compact=ultra&apiKey=8f4fa64869b721ef3b4b"
    
    get_res_cc = requests.get(api_convert_currency)
    
    if get_res_cc .status_code == 200:
        val = (json.loads(get_res_cc.content.decode('utf-8')))
        covert_currency = val["USD_IDR"]
        
     
    
    api_jds_product= f"https://60c18de74f7e880017dbfd51.mockapi.io/api/v1/jabar-digital-services/product"
    
    get_res_jds = requests.get(api_jds_product)
    
    new_result = []
    if get_res_jds .status_code == 200:
        jds_product = (json.loads(get_res_jds.content.decode('utf-8')))
        
        for val in jds_product:
                data = {
                            'id'         : val["id"],
                            'createdAt'  : val["createdAt"],
                            'priceUSD'   : val["price"],
                            'priceIDR'   : "%.2f" % (float(val["price"]) * float(covert_currency)) ,
                            'department' : val["department"],
                            'product'    : val["product"]
                       }
                
                new_result.append(data)
                
    return { 
             'status': True,
             'data'  : new_result
            }


def custom_openapi():
    if app.openapi_schema:
        return app.openapi_schema
    openapi_schema = get_openapi(
        title="JDS - Ahmad Fauzi Iskandar",
        version="2.5.0",
        description="Authentication App & Fetch App With Laravel",
        routes=app.routes,
    )
    openapi_schema["info"]["x-logo"] = {
        "url": "https://fastapi.tiangolo.com/img/logo-margin/logo-teal.png"
    }
    app.openapi_schema = openapi_schema
    return app.openapi_schema


app.openapi = custom_openapi
